// SPDX-FileCopyrightText: Copyright 2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-FileCopyrightText: Copyright 2014-2015 Stripe, Inc.
//
// SPDX-License-Identifier: Apache-2.0

package urlencoded

import (
	"fmt"
	"reflect"
	"strconv"
)

const (
	alphaDelta = 'a' - 'A'
)

// convert like this: "hello_world" to "helloWorld"
func _LowerCamelCasedName(in string) string {
	return _TitleCasedName(in, true)
}

// convert like this: "hello_world" to "HelloWorld"
func _TitleCasedName(name string, lower bool) string {
	newstr := make([]rune, 0, len(name))
	upNextChar := true

	for _, chr := range name {
		if chr == '-' || chr == '_' || chr == '.' {
			upNextChar = true
			continue
		}

		if chr >= 'a' && chr <= 'z' && upNextChar {
			chr -= alphaDelta
		}

		newstr = append(newstr, chr)
		upNextChar = false
	}

	if lower && newstr[0] >= 'A' && newstr[0] <= 'Z' {
		newstr[0] += alphaDelta
	}

	return string(newstr)
}

// StringSlice converts a slice of string values into a slice of
// string pointers
func _StringSlice(src []string) []*string {
	dst := make([]*string, len(src))
	for i := 0; i < len(src); i++ {
		dst[i] = &(src[i])
	}
	return dst
}

// StringValueSlice converts a slice of string pointers into a slice of
// string values
func _StringValueSlice(src []*string) []string {
	dst := make([]string, len(src))
	for i := 0; i < len(src); i++ {
		if src[i] != nil {
			dst[i] = *(src[i])
		}
	}
	return dst
}

type _Parser interface {
	Parse(in string) error
}

func _SetValue(rv reflect.Value, data []string) error {
	rt := rv.Type()
	if len(data) == 0 {
		return nil
	}

	// Dereference ptr
	_PrepareValue(rv, rt)

	// rv must can interface
	if v, ok := rv.Interface().(_Parser); ok {
		return v.Parse(data[0])
	}

	if rv.Kind() == reflect.Ptr {
		// fmt.Printf("rt %s is ptr, derefreence\n", rt.String())
		rv = rv.Elem()
		rt = rv.Type()
	}

	switch rv.Kind() {
	case reflect.String:
		rv.SetString(data[0])

	case reflect.Bool:
		rv.SetBool(data[0] == "true")

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if v, err := strconv.ParseInt(data[0], 10, 64); err != nil {
			return fmt.Errorf("arg %s as int: %s", rt.Name(), err.Error())
		} else {
			rv.SetInt(v)
		}
	case reflect.Float32, reflect.Float64:
		if v, err := strconv.ParseFloat(data[0], 64); err != nil {
			return fmt.Errorf("arg %s as float: %s", rt.Name(), err.Error())
		} else {
			rv.SetFloat(v)
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		if v, err := strconv.ParseUint(data[0], 10, 64); err != nil {
			return fmt.Errorf("arg %s as uint: %s", rt.Name(), err.Error())
		} else {
			rv.SetUint(v)
		}
	case reflect.Slice:
		typeName := rt.Elem().String()
		if typeName == "string" {
			rv.Set(reflect.ValueOf(data))
		} else if typeName == "*string" {
			rv.Set(reflect.ValueOf(_StringSlice(data)))
		} else {
			return fmt.Errorf("unsupported type scan %s slice", typeName)
		}
	default:
		return fmt.Errorf("unsupported type scan %s", rt.String())
	}
	return nil
}

func _GetValue(rv reflect.Value) (data []string, err error) {
	rv = reflect.Indirect(rv)
	rt := rv.Type()
	switch rv.Kind() {
	case reflect.String, reflect.Bool, reflect.Int, reflect.Int8,
		reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint,
		reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:

		return []string{fmt.Sprintf("%v", rv.Interface())}, nil
	case reflect.Slice:
		typeName := rt.Elem().String()
		if typeName == "string" {
			return rv.Interface().([]string), nil
		} else if typeName == "*string" {
			return _StringValueSlice(rv.Interface().([]*string)), nil
		}
		return nil, fmt.Errorf("unsupported type: %s %s", rt, rv.Kind())
	default:
		return nil, fmt.Errorf("unsupported type: %s %s", rt, rv.Kind())
	}
}

func _PrepareValue(rv reflect.Value, rt reflect.Type) {
	if rv.Kind() == reflect.Ptr && rv.IsNil() {
		rv.Set(reflect.New(rt.Elem()))
	}
}
