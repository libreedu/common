// SPDX-FileCopyrightText: Copyright (c) 2017 Fadhli Dzil Ikram
// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: LGPL-3.0-or-later

// The colorful and simple logging library
package log

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"runtime/debug"
	"strings"
	"sync"
	"time"

	"codeberg.org/libreedu/common/log/colorful"
	"golang.org/x/crypto/ssh/terminal"
)

// Mainly this just looks cool.
//
// Logger IOStreams are syntactically similar to C++ iostream to stdout:
// `std::cout << "Words\n"` becomes `logger.Info() <- "Words"`
//
// This may be cool for CGO with C++ (if you're crazy)!
type IOStream chan<- interface{}

func loggerIOStream(l *Logger, p Prefix, caller string, debugq bool) IOStream {
	iostream := make(chan interface{}, 0)
	go func() {
		for {
			m := <-iostream
			if !debugq || l.DebugOut {
				l.Output(1, p, fmt.Sprintf("%v\n", m), caller)
			}
		}
	}()
	return iostream
}

// FdWriter interface extends existing io.Writer with file descriptor
// function support
type FdWriter interface {
	io.Writer
	Fd() uintptr
}

// Logger defines the underlying storage for single logger.
//
// IOStreams are cached on first call to their functions.
type Logger struct {
	Name      string
	mu        sync.RWMutex
	Color     bool
	out       io.Writer
	DebugOut  bool
	Timestamp bool
	Quiet     bool
	buf       colorful.ColorBuffer

	fatal IOStream
	error IOStream
	warn  IOStream
	info  IOStream
	debug IOStream
	trace IOStream
}

func formatCaller(pc uintptr, file string, line int, ok bool) string {
	file = filepath.Base(file)
	fn := runtime.FuncForPC(pc).Name()
	return fmt.Sprintf("%s:%s:%d", fn, file, line)
}

// Prefix struct define plain and color byte
type Prefix struct {
	Plain []byte
	Color []byte
	File  bool
}

var (
	PanicPrefix = Prefix{
		Plain: []byte("[PANIC] "),
		Color: append(append([]byte{'['}, colorful.Red([]byte("PANIC"))...), []byte("] ")...),
		File:  false,
	}

	// FatalPrefix show fatal prefix
	FatalPrefix = Prefix{
		Plain: []byte("[FATAL] "),
		Color: append(append([]byte{'['}, colorful.Red([]byte("FATAL"))...), []byte("] ")...),
		File:  true,
	}

	// ErrorPrefix show error prefix
	ErrorPrefix = Prefix{
		Plain: []byte("[ERROR] "),
		Color: append(append([]byte{'['}, colorful.Red([]byte("ERROR"))...), []byte("] ")...),
		File:  true,
	}

	// WarnPrefix show warn prefix
	WarnPrefix = Prefix{
		Plain: []byte("[WARN]  "),
		Color: append(append([]byte{'['}, colorful.Orange([]byte("WARN"))...), []byte("]  ")...),
	}

	// InfoPrefix show info prefix
	InfoPrefix = Prefix{
		Plain: []byte("[INFO]  "),
		Color: append(append([]byte{'['}, colorful.Green([]byte("INFO"))...), []byte("]  ")...),
	}

	// DebugPrefix show info prefix
	DebugPrefix = Prefix{
		Plain: []byte("[DEBUG] "),
		Color: append(append([]byte{'['}, colorful.Purple([]byte("DEBUG"))...), []byte("] ")...),
		File:  true,
	}

	// TracePrefix show info prefix
	TracePrefix = Prefix{
		Plain: []byte("[TRACE] "),
		Color: append(append([]byte{'['}, colorful.Cyan([]byte("TRACE"))...), []byte("] ")...),
	}
)

// New returns new Logger instance with predefined writer output and
// automatically detect terminal coloring support
func New(out io.Writer, name string) *Logger {
	l := Logger{
		Name:      name,
		Color:     reflect.TypeOf(out).Implements(reflect.TypeOf((*FdWriter)(nil)).Elem()) && terminal.IsTerminal(int(out.(FdWriter).Fd())),
		out:       out,
		Timestamp: true,
	}
	return &l
}

// Output print the actual value
func (l *Logger) Output(depth int, prefix Prefix, data string, caller string) error {
	// Check if quiet is requested, and try to return no error and be
	// quiet
	if l.Quiet {
		return nil
	}
	// Get current time
	now := time.Now()
	// Acquire exclusive access to the shared buffer
	l.mu.Lock()
	defer l.mu.Unlock()
	// Reset buffer so it start from the begining
	l.buf.Reset()
	// Write prefix to the buffer
	if l.Color {
		l.buf.Append(prefix.Color)
	} else {
		l.buf.Append(prefix.Plain)
	}
	// Check if the log require timestamping
	if l.Timestamp {
		// Print timestamp color if color enabled
		if l.Color {
			l.buf.Blue()
		}
		// Print date and time
		year, month, day := now.Date()
		l.buf.AppendInt(year, 4)
		l.buf.AppendByte('/')
		l.buf.AppendInt(int(month), 2)
		l.buf.AppendByte('/')
		l.buf.AppendInt(day, 2)
		l.buf.AppendByte(' ')
		hour, min, sec := now.Clock()
		l.buf.AppendInt(hour, 2)
		l.buf.AppendByte(':')
		l.buf.AppendInt(min, 2)
		l.buf.AppendByte(':')
		l.buf.AppendInt(sec, 2)
		l.buf.AppendByte(' ')
		// Print reset color if color enabled
		if l.Color {
			l.buf.Off()
		}
	}
	// Add caller filename and line if enabled
	if prefix.File {
		// Print color start if enabled
		if l.Color {
			l.buf.Orange()
		}
		if caller == "" {
			l.buf.Append([]byte(formatCaller(runtime.Caller(depth + 1))))
		} else {
			l.buf.Append([]byte(caller))
		}
		l.buf.AppendByte(' ')
		// Print color stop
		if l.Color {
			l.buf.Off()
		}
	}

	// Print the logger's name
	if l.Color {
		l.buf.Gray()
	}
	l.buf.Append([]byte(l.Name + ": "))
	if l.Color {
		l.buf.Off()
	}

	// Print the actual string data from caller
	l.buf.Append([]byte(data))
	if len(data) == 0 || data[len(data)-1] != '\n' {
		l.buf.AppendByte('\n')
	}
	// Flush buffer to output
	_, err := l.out.Write(l.buf.Buffer)
	return err
}

// A function that recovers and writes pretty panic messages. Usage:
// `defer Recover()(true)` to exit, false argument to continue running.
func (l *Logger) Recover() func(exit bool) {
	root := (formatCaller(runtime.Caller(1)))
	return func(exit bool) {
		P := recover()
		if P == nil {
			return
		}

		out := loggerIOStream(l, PanicPrefix, root, false)
		panicfn := []byte(formatCaller(runtime.Caller(2)))
		if l.Color {
			panicfn = colorful.Orange(panicfn)
			root = string(colorful.Orange([]byte(root)))
		}
		s := strings.Split(string(debug.Stack()), "\n")
		if exit {
			out <- fmt.Sprintf("Fatal panic from %s: %v", panicfn, P)
		} else {
			out <- fmt.Sprintf("Handled panic from %s: %v", panicfn, P)
		}
		out <- "Using defer at " + root
		out <- "Stack trace:"
		for _, v := range s[7 : len(s)-1] {
			out <- "\t" + v
		}

		if exit {
			os.Exit(1)
		}
	}
}

func (l *Logger) Fatal() IOStream {
	caller := formatCaller(runtime.Caller(1))
	iostream := make(chan interface{})
	go func() {
		for t := range iostream {
			l.Output(1, FatalPrefix, fmt.Sprintf("%v\n", t), caller)
			os.Exit(1)
		}
	}()
	return iostream
}

func (l *Logger) Error() IOStream {
	if l.error == nil {
		l.error = loggerIOStream(l, ErrorPrefix, formatCaller(runtime.Caller(1)), false)
	}
	return l.error
}

func (l *Logger) Warn() IOStream {
	if l.warn == nil {
		l.warn = loggerIOStream(l, WarnPrefix, formatCaller(runtime.Caller(1)), false)
	}
	return l.warn
}

func (l *Logger) Info() IOStream {
	if l.info == nil {
		l.info = loggerIOStream(l, InfoPrefix, formatCaller(runtime.Caller(1)), false)
	}
	return l.info
}

func (l *Logger) Debug() IOStream {
	if l.debug == nil {
		l.debug = loggerIOStream(l, DebugPrefix, formatCaller(runtime.Caller(1)), true)
	}
	return l.debug
}

func (l *Logger) Trace() IOStream {
	if l.trace == nil {
		l.trace = loggerIOStream(l, TracePrefix, formatCaller(runtime.Caller(1)), true)
	}
	return l.trace
}

// Fatalf print formatted fatal message to output and quit the
// application with status 1
func (l *Logger) Fatalf(format string, v ...interface{}) {
	l.Output(1, FatalPrefix, fmt.Sprintf(format, v...), "")
	os.Exit(1)
}

// Errorf print formatted error message to output
func (l *Logger) Errorf(format string, v ...interface{}) {
	l.Output(1, ErrorPrefix, fmt.Sprintf(format, v...), "")
}

// Warnf print formatted warning message to output
func (l *Logger) Warnf(format string, v ...interface{}) {
	l.Output(1, WarnPrefix, fmt.Sprintf(format, v...), "")
}

// Infof print formatted informational message to output
func (l *Logger) Infof(format string, v ...interface{}) {
	l.Output(1, InfoPrefix, fmt.Sprintf(format, v...), "")
}

// Debugf print formatted debug message to output if debug output
// enabled
func (l *Logger) Debugf(format string, v ...interface{}) {
	if l.DebugOut {
		l.Output(1, DebugPrefix, fmt.Sprintf(format, v...), "")
	}
}

// Tracef print formatted trace message to output if debug output
// enabled
func (l *Logger) Tracef(format string, v ...interface{}) {
	if l.DebugOut {
		l.Output(1, TracePrefix, fmt.Sprintf(format, v...), "")
	}
}
