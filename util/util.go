// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: MIT

package util

import (
	"crypto/rand"
	"io"
	mrand "math/rand"
	"net/http"
	"time"
	"unsafe"

	"codeberg.org/libreedu/common/encoding/urlencoded"
	json "github.com/json-iterator/go"
)

// Marshals `j` and prints on `w` with `Content-Type: application/json`
func JSON(w http.ResponseWriter, j interface{}, status int) {
	result, err := json.Marshal(j)
	if err != nil {
		w.WriteHeader(500)
		http.Error(w, http.StatusText(500), 500)
		return
	}
	if status != 0 {
		w.WriteHeader(status)
	}
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Security-Policy", "connect-src https:;")
	w.Write(result)
}

// Unmarshals body of `r` as JSON to `&out`. If error, prints error to
// `w` and returns `false`.
func ParseJSON(w http.ResponseWriter, r *http.Request, out interface{}) bool {
	if r.Header.Get("Content-Type") != "application/json" {
		JSON(w, map[string]string{"error": "expected application/json"}, 415)
		return false
	}

	b, err := io.ReadAll(r.Body)
	if err != nil {
		JSON(w, map[string]string{"error": "unknown"}, 500)
		return false
	}
	if (json.Unmarshal(b, out)) != nil {
		JSON(w, map[string]string{"error": "expected application/json"}, 415)
		return false
	}

	return true
}

// Unmarshals body of `r` as x-www-form-urlencoded to `&out`. If error,
// prints error to `w` and returns `false`.
func ParseForm(w http.ResponseWriter, r *http.Request, out interface{}) bool {
	b, err := io.ReadAll(r.Body)
	if err != nil {
		return false
	}

	return urlencoded.Unmarshal(b, out) == nil
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandSeq(n int) string {
	mrand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[mrand.Intn(len(letters))]
	}
	return string(b)
}

func CryptoRandSeq(n int) string {
	ns := make([]uint8, n)
	_, err := rand.Read(*(*[]byte)(unsafe.Pointer(&ns)))
	if err != nil {
		panic(err)
	}

	runes := make([]rune, n)
	for i, n := range ns {
		runes[i] = letters[int(n)%(len(letters)-1)]
	}
	return string(runes)
}

// Returns all items from `src` that also occur in `list` in a slice.
func FilterIncludes[T comparable](src []T, list []T) []T {
	new := []T{}
	for _, s := range src {
		for _, v := range list {
			if s == v {
				new = append(new, s)
			}
		}
	}
	return new
}
