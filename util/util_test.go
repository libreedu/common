// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: MIT

package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCryptoRandSeq(t *testing.T) {
	for i := int64(0); i < 1<<20; i++ {
		seq := []rune(CryptoRandSeq(64))
		assert.Equal(t, len(seq), len(FilterIncludes(letters, seq)))
	}
}
